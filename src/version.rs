use libedgegrid::version as libver;

include!(concat!(env!("OUT_DIR"), "/version.rs"));

#[cfg(unix)]
fn verbose_ver() -> String {
    format!("\x1b[32;1megc {}\x1b[0m ({} {}) (built {})\ncommit-hash: {}\ncommit-date: \
             {}\nbuild-date: {}\nhost: {}\nrelease: {}\n\n{}",
            semver(),
            short_sha(),
            commit_date(),
            short_now(),
            sha(),
            commit_date(),
            short_now(),
            target(),
            semver(),
            libver::version(true))
}

#[cfg(windows)]
fn verbose_ver() -> String {
    format!("egc {} ({} {}) (built {})\ncommit-hash: {}\ncommit-date: {}\nbuild-date: {}\nhost: \
             {}\nrelease: {}\n\n{}",
            semver(),
            short_sha(),
            commit_date(),
            short_now(),
            sha(),
            commit_date(),
            short_now(),
            target(),
            semver(),
            libver::version(true))
}

#[cfg(unix)]
fn ver() -> String {
    format!("\x1b[32;1megc {}\x1b[0m ({} {}) (built {})\n{}",
            semver(),
            short_sha(),
            commit_date(),
            short_now(),
            libver::version(false))
}

#[cfg(windows)]
fn ver() -> String {
    format!("egc {}[0m ({} {}) (built {})\n{}",
            semver(),
            short_sha(),
            commit_date(),
            short_now(),
            libver::version(false))
}

pub fn version(verbose: bool) -> String {
    if verbose {
        verbose_ver()
    } else {
        ver()
    }
}

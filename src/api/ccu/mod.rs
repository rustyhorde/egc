use docopt::Docopt;
use libedgegrid::ccu::req::{self, PurgeArls, PurgeCPCodes};
use libedgegrid::ccu::resp::{self, CCUResponseType};
use std::fs::File;
use std::io::{BufRead, BufReader};

#[cfg_attr(rustfmt, rustfmt_skip)]
static USAGE: &'static str = "ccu - Run the Akamai EdgeGrid Client Content Control Utility API

Usage:
    ccu [options] purge <queuename> [<arlorcp>...]
    ccu [options] qlen <queuename>
    ccu [options] status <purgeid>

Options:
    -c --cpcode         Enable a cpcode purge.
    -a AF --arlfile=AF  Read arls from a file instead of the command line.
    -t MS --timeout=MS  Set the curl timeout for the request in milliseconds.

Examples:
    N/A";

#[derive(Debug, RustcDecodable)]
pub struct CCUArgs {
    arg_purgeid: String,
    arg_queuename: String,
    arg_arlorcp: Vec<String>,
    cmd_purge: bool,
    cmd_qlen: bool,
    cmd_status: bool,
    flag_cpcode: bool,
    flag_arlfile: String,
    flag_timeout: usize,
}

pub fn usage<'a>() -> &'a str {
    USAGE
}

pub fn parse_args(args: Vec<String>, auths: &mut ::AuthsHashMap) -> ! {
    let argv = || args;
    let ccuargs: CCUArgs = Docopt::new(USAGE)
                               .and_then(|d| d.argv(argv().into_iter()).decode())
                               .unwrap_or_else(|e| e.exit());

    if ccuargs.cmd_qlen {
        queue_length(&ccuargs, auths)
    } else if ccuargs.cmd_purge {
        purge(&ccuargs, auths)
    } else if ccuargs.cmd_status {
        status(&ccuargs, auths)
    } else {
        ::exit(String::from("Unknown command!"), 1);
    }
}

pub fn queue_length(args: &CCUArgs, auths: &mut ::AuthsHashMap) -> ! {
    let egr = ::get_auth("ccu", args.flag_timeout, auths);
    match req::queue_length(&egr, &args.arg_queuename[..]) {
        Ok(r) => {
            debug!("{:?}", r);
            match resp::parse(r, CCUResponseType::QueueLength) {
                Ok(o) => ::exit(o, 0),
                Err(e) => ::exit(format!("{}", e), 1),
            }
        }
        Err(e) => ::exit(format!("{}", e), 1),
    }
}

pub fn purge(args: &CCUArgs, auths: &mut ::AuthsHashMap) -> ! {
    let egr = ::get_auth("ccu", args.flag_timeout, auths);

    let mut arls: Vec<String> = Vec::new();

    if args.flag_arlfile.is_empty() {
        arls.extend(args.arg_arlorcp.clone().into_iter());
    } else {
        match File::open(&args.flag_arlfile) {
            Ok(f) => {
                let br = BufReader::new(f);
                for line in br.lines() {
                    if let Ok(l) = line {
                        arls.push(l);
                    }
                }
            }
            Err(e) => ::exit(format!("{}", e), 1),
        }
    };

    if !args.flag_cpcode {
        let body = PurgeArls::new(arls);

        match req::purge_by_arl(&egr, &args.arg_queuename[..], body) {
            Ok(r) => {
                debug!("{:?}", r);
                match resp::parse(r, CCUResponseType::Purge) {
                    Ok(o) => ::exit(o, 0),
                    Err(e) => ::exit(format!("{}", e), 1),
                }
            }
            Err(e) => ::exit(format!("{}", e), 1),
        }
    } else {
        let body = PurgeCPCodes::new(arls);

        match req::purge_by_cpcode(&egr, &args.arg_queuename[..], body) {
            Ok(r) => {
                debug!("{:?}", r);
                match resp::parse(r, CCUResponseType::Purge) {
                    Ok(o) => ::exit(o, 0),
                    Err(e) => ::exit(format!("{}", e), 1),
                }
            }
            Err(e) => ::exit(format!("{}", e), 1),
        }
    }
}

pub fn status(args: &CCUArgs, auths: &mut ::AuthsHashMap) -> ! {
    let egr = ::get_auth("ccu", args.flag_timeout, auths);
    match req::purge_status(&egr, &args.arg_purgeid[..]) {
        Ok(r) => {
            debug!("{:?}", r);
            match resp::parse(r, CCUResponseType::PurgeStatus) {
                Ok(o) => ::exit(o, 0),
                Err(e) => ::exit(format!("{}", e), 1),
            }
        }
        Err(e) => ::exit(format!("{}", e), 1),
    }
}

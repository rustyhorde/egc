pub mod alert;
// pub mod events;
pub mod dt;
// pub mod lds;

// static USAGE: &'static str = "egc - Run the Akamai EdgeGrid Client
//
// Usage:
//     egc [options] events get <accountid>
//     egc [options] lds get configs
//     egc [options] lds (get | delete | suspend | resume) config <serviceid>
//     egc [options] lds copy <serviceid> <objid> <objtype> <pgid>
//     egc [options] lds post config <servicdid> <confpost>
//     egc [options] lds put config <servicdid> <confput>
//     egc [options] lds get dict <dictname> [<oobjtype> <oobjid> <ckv>]

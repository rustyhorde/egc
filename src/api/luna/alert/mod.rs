use docopt::Docopt;
use libedgegrid::luna::alert::req::{self, AlertPostData};
use libedgegrid::luna::alert::resp::{self, AlertResponseType};

#[cfg_attr(rustfmt, rustfmt_skip)]
static USAGE: &'static str = "alert - Run the Akamai EdgeGrid Alert API

Usage:
    alert [options] -i <alertid>
    alert [options] (get|post) [<cpcode>...]

Options:
    -i --byid           Lookup the alert by id.
    -t MS --timeout=MS  Set the curl timeout for the request in milliseconds.

Examples:
    N/A";

#[derive(Debug, RustcDecodable)]
pub struct AlertArgs {
    arg_alertid: String,
    arg_cpcode: Option<Vec<String>>,
    cmd_get: bool,
    cmd_post: bool,
    flag_timeout: usize,
    flag_byid: bool,
}

pub fn usage<'a>() -> &'a str {
    USAGE
}

pub fn parse_args(args: Vec<String>, auths: &mut ::AuthsHashMap) -> ! {
    let argv = || args;
    let args: AlertArgs = Docopt::new(USAGE)
                              .and_then(|d| d.argv(argv().into_iter()).decode())
                              .unwrap_or_else(|e| e.exit());

    if args.flag_byid {
        get_by_id(&args, auths)
    } else if args.cmd_get {
        get(&args, auths)
    } else if args.cmd_post {
        post(&args, auths)
    } else {
        ::exit(String::from("Unknown command!"), 1);
    }
}

pub fn get_by_id(args: &AlertArgs, auths: &mut ::AuthsHashMap) -> ! {
    let egr = ::get_auth("luna", args.flag_timeout, auths);
    match req::get_by_id(&egr, &args.arg_alertid) {
        Ok(r) => {
            debug!("{:?}", r);
            match resp::parse(r, AlertResponseType::GetById) {
                Ok(o) => ::exit(o, 0),
                Err(e) => ::exit(format!("{}", e), 1),
            }
        }
        Err(e) => ::exit(format!("{}", e), 1),
    }
}

pub fn get(args: &AlertArgs, auths: &mut ::AuthsHashMap) -> ! {
    let egr = ::get_auth("luna", args.flag_timeout, auths);
    match req::get(&egr, &args.arg_cpcode) {
        Ok(r) => {
            debug!("{:?}", r);
            match resp::parse(r, AlertResponseType::Get) {
                Ok(o) => ::exit(o, 0),
                Err(e) => ::exit(format!("{}", e), 1),
            }
        }
        Err(e) => ::exit(format!("{}", e), 1),
    }
}

pub fn post(args: &AlertArgs, auths: &mut ::AuthsHashMap) -> ! {
    let egr = ::get_auth("luna", args.flag_timeout, auths);
    let body = AlertPostData::new(args.arg_cpcode.clone());
    match req::post(&egr, body) {
        Ok(r) => {
            debug!("{:?}", r);
            match resp::parse(r, AlertResponseType::Post) {
                Ok(o) => ::exit(o, 0),
                Err(e) => ::exit(format!("{}", e), 1),
            }
        }
        Err(e) => ::exit(format!("{}", e), 1),
    }
}

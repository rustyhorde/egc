use libedgegrid::{auth, luna};
use std::collections::HashMap;
use std::default::Default;

pub fn copy_config(args: &::Args, auths: &mut HashMap<String, auth::EdgeGridAuth>) -> ! {
    let egr = ::get_auth("luna", args, auths);

    match luna::lds::copy_config(&egr,
                                 &args.arg_serviceid,
                                 &args.arg_objid,
                                 &args.arg_objtype,
                                 args.arg_pgid) {
        Ok(resp) => println!("{:?}", resp),
        Err(e) => ::exit(format!("{:?}", e), 1),
    }
    ::exit(String::new(), 0)
}

pub fn delete_config(args: &::Args, auths: &mut HashMap<String, auth::EdgeGridAuth>) -> ! {
    let egr = ::get_auth("luna", args, auths);

    match luna::lds::delete_config(&egr, &args.arg_serviceid) {
        Ok(resp) => println!("{:?}", resp),
        Err(e) => ::exit(format!("{:?}", e), 1),
    }
    ::exit(String::new(), 0)
}

pub fn get_config(args: &::Args, auths: &mut HashMap<String, auth::EdgeGridAuth>) -> ! {
    let egr = ::get_auth("luna", args, auths);

    match luna::lds::get_config(&egr, &args.arg_serviceid) {
        Ok(resp) => println!("{:?}", resp),
        Err(e) => ::exit(format!("{:?}", e), 1),
    }
    ::exit(String::new(), 0)
}

pub fn get_configs(args: &::Args, auths: &mut HashMap<String, auth::EdgeGridAuth>) -> ! {
    let egr = ::get_auth("luna", args, auths);

    match luna::lds::get_configs(&egr) {
        Ok(resp) => println!("{:?}", resp),
        Err(e) => ::exit(format!("{:?}", e), 1),
    }
    ::exit(String::new(), 0)
}

pub fn post_config(args: &::Args, auths: &mut HashMap<String, auth::EdgeGridAuth>) -> ! {
    let egr = ::get_auth("luna", args, auths);

    let ppcd = Default::default();

    match luna::lds::post_config(&egr, &args.arg_serviceid, &ppcd) {
        Ok(resp) => println!("{:?}", resp),
        Err(e) => ::exit(format!("{:?}", e), 1),
    }
    ::exit(String::from("Not yet implemented!"), 0)
}

pub fn put_config(args: &::Args, auths: &mut HashMap<String, auth::EdgeGridAuth>) -> ! {
    let egr = ::get_auth("luna", args, auths);

    let ppcd = Default::default();

    match luna::lds::put_config(&egr, &args.arg_serviceid, &ppcd) {
        Ok(resp) => println!("{:?}", resp),
        Err(e) => ::exit(format!("{:?}", e), 1),
    }
    ::exit(String::from("Not yet implemented!"), 0)
}

pub fn resume_config(args: &::Args, auths: &mut HashMap<String, auth::EdgeGridAuth>) -> ! {
    let egr = ::get_auth("luna", args, auths);

    match luna::lds::resume_config(&egr, &args.arg_serviceid) {
        Ok(resp) => println!("{:?}", resp),
        Err(e) => ::exit(format!("{:?}", e), 1),
    }
    ::exit(String::new(), 0)
}

pub fn suspend_config(args: &::Args, auths: &mut HashMap<String, auth::EdgeGridAuth>) -> ! {
    let egr = ::get_auth("luna", args, auths);

    match luna::lds::suspend_config(&egr, &args.arg_serviceid) {
        Ok(resp) => println!("{:?}", resp),
        Err(e) => ::exit(format!("{:?}", e), 1),
    }
    ::exit(String::new(), 0)
}

pub fn get_dict(args: &::Args, auths: &mut HashMap<String, auth::EdgeGridAuth>) -> ! {
    let egr = ::get_auth("luna", args, auths);

    let objtype = match args.arg_oobjtype {
        Some(ref s) => Some(s),
        None => None,
    };

    let ckv = match args.arg_ckv {
        Some(ref s) => Some(s),
        None => None,
    };

    match luna::lds::get_dict(&egr, &args.arg_dictname, objtype, args.arg_oobjid, ckv) {
        Ok(resp) => println!("{:?}", resp),
        Err(e) => ::exit(format!("{:?}", e), 1),
    }
    ::exit(String::new(), 0)
}

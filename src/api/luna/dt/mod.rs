use docopt::Docopt;
use libedgegrid::EdgeGridResponse;
use libedgegrid::auth::EdgeGridAuth;
use libedgegrid::luna::dt::req;
use libedgegrid::luna::dt::resp::{self, DTResponseType};

#[cfg_attr(rustfmt, rustfmt_skip)]
static USAGE: &'static str = "dt - Run the Akamai EdgeGrid Diagnostic Tools

Usage:
    dt [options] dig <hostname> <location> <querytype>
    dt [options] errtranslate <errorcode>
    dt [options] geo <ip>
    dt [options] locs
    dt [options] mtr <hostname> <location>
    dt [options] translate <url>
    dt [options] verify <ip>

Options:
    -t MS --timeout=MS  Set the curl timeout for the request in milliseconds.

Examples:
    N/A";

#[derive(RustcDecodable)]
pub struct DTArgs {
    arg_errorcode: String,
    arg_hostname: String,
    arg_ip: String,
    arg_location: String,
    arg_querytype: String,
    arg_url: String,
    cmd_dig: bool,
    cmd_errtranslate: bool,
    cmd_geo: bool,
    cmd_locs: bool,
    cmd_mtr: bool,
    cmd_translate: bool,
    cmd_verify: bool,
    flag_timeout: usize,
}

pub fn usage<'a>() -> &'a str {
    USAGE
}

pub fn parse_args(args: Vec<String>, auths: &mut ::AuthsHashMap) -> ! {
    let argv = || args;
    let args: DTArgs = Docopt::new(USAGE)
                           .and_then(|d| d.argv(argv().into_iter()).decode())
                           .unwrap_or_else(|e| e.exit());
    let egr = ::get_auth("luna", args.flag_timeout, auths);
    let mut rt = DTResponseType::Dig;

    let req = if args.cmd_dig {
        dig(&args, &egr)
    } else if args.cmd_errtranslate {
        rt = DTResponseType::ErrorTranslate;
        error_translate(&args, &egr)
    } else if args.cmd_geo {
        rt = DTResponseType::Geo;
        geo(&args, &egr)
    } else if args.cmd_locs {
        rt = DTResponseType::Locations;
        locations(&args, &egr)
    } else if args.cmd_mtr {
        rt = DTResponseType::Mtr;
        mtr(&args, &egr)
    } else if args.cmd_translate {
        rt = DTResponseType::Translate;
        translate(&args, &egr)
    } else if args.cmd_verify {
        rt = DTResponseType::Verify;
        verify(&args, &egr)
    } else {
        ::exit(String::from("Unknown command!"), 1);
    };

    match req {
        Ok(r) => {
            debug!("{:?}", r);
            match resp::parse(r, rt) {
                Ok(o) => ::exit(o, 0),
                Err(e) => ::exit(format!("{}", e), 1),
            }
        }
        Err(e) => ::exit(format!("{}", e), 1),
    }
}

fn dig(args: &DTArgs, egr: &EdgeGridAuth) -> EdgeGridResponse {
    req::dig(&egr,
             &args.arg_hostname[..],
             &args.arg_location[..],
             &args.arg_querytype[..])
}

fn error_translate(args: &DTArgs, egr: &EdgeGridAuth) -> EdgeGridResponse {
    req::error_translate(&egr, &args.arg_errorcode[..])
}

fn geo(args: &DTArgs, egr: &EdgeGridAuth) -> EdgeGridResponse {
    req::geo(&egr, &args.arg_ip[..])
}

fn locations(_: &DTArgs, egr: &EdgeGridAuth) -> EdgeGridResponse {
    req::locations(&egr)
}

fn mtr(args: &DTArgs, egr: &EdgeGridAuth) -> EdgeGridResponse {
    req::mtr(&egr, &args.arg_hostname[..], &args.arg_location[..])
}

fn translate(args: &DTArgs, egr: &EdgeGridAuth) -> EdgeGridResponse {
    req::translate(&egr, &args.arg_url[..])
}

fn verify(args: &DTArgs, egr: &EdgeGridAuth) -> EdgeGridResponse {
    req::verify(&egr, &args.arg_ip[..])
}

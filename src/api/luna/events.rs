use libedgegrid::{auth, luna};
use std::collections::HashMap;

pub fn get_by_account_id(args: &::Args, auths: &mut HashMap<String, auth::EdgeGridAuth>) -> ! {
    let egr = ::get_auth("luna", args, auths);

    match luna::events::get_by_account_id(&egr, &args.arg_accountid) {
        Ok(resp) => println!("{:?}", resp),
        Err(e) => ::exit(format!("{:?}", e), 1),
    }
    ::exit(String::new(), 0)
}
